//*******************************************************************************
//* Copyright (c) 2008-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************/
// ============================================================================
//
// = CONTEXT
//		SpectrometerTask
//
// = File
//		SpectrometerTask.h
//
// = AUTHOR
//    S.Gara
//
// ============================================================================

#ifndef _SPECTROMETER_TASK_H_
#define _SPECTROMETER_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>

#include <yat/any/GenericContainer.h>
#include <yat/memory/DataBuffer.h>
#include <yat/memory/SharedPtr.h>
#include <yat/memory/UniquePtr.h>
#include <yat/time/Time.h>
#include <yat4tango/DeviceTask.h>
#include <yat4tango/DynamicAttributeManager.h>

#ifndef NO_DRIVER
  #include <include/Wrapper.h>
#endif

namespace Spectrometer_ns
{

const unsigned short internalSingle   = 0;
const unsigned short externalSingle   = 2;

typedef yat::Buffer<double> DoubleBuf;
typedef YAT_SHARED_PTR(DoubleBuf) DoubleBufPtr;

// ============================================================================
// class: SpectrometerTask
// ============================================================================
class SpectrometerTask : public yat4tango::DeviceTask
{
public:

  //- the task's configuration data struct
  typedef struct Config 
  {
    //-default ctor
    Config ()
      : task_activity_period_ms(100), 
        host_device(0),
#ifndef NO_DRIVER
		wrapper(),
#endif
		spectrometer_index(0),
    stop_acq_on_saturation(true),
    alarm_on_saturation(false),
    nb_gates(0)
    {}

    //- copy ctor
    Config (const Config & src)
    { 
      *this = src;
    }

    //- operator=
    const Config & operator= (const Config & src)
    { 
      if (&src == this)
        return *this;
      
      task_activity_period_ms = src.task_activity_period_ms;
      host_device = src.host_device;

#ifndef NO_DRIVER
      wrapper = src.wrapper;
#endif
      spectrometer_index = src.spectrometer_index;
      stop_acq_on_saturation = src.stop_acq_on_saturation;
      alarm_on_saturation = src.alarm_on_saturation;

      return *this;
    }

    //- period of task's periodic activity in msecs 
    size_t task_activity_period_ms;

    //- the Tango device hosting this task
    Tango::DeviceImpl * host_device;
  
  #ifndef NO_DRIVER
    //- wrapper object
    Wrapper wrapper;
  #endif

    //- spectrometer index
    unsigned short spectrometer_index;

    //- saturation flag
    bool stop_acq_on_saturation;

    //- Alarm flag
    bool alarm_on_saturation;

    //- Number of gates
    std::size_t nb_gates;

  } Config;

   //- the data return values for monitoring
  typedef struct DataReturnValues 
  {
    //-default ctor
    DataReturnValues ()
      : intensity(0),
	   intensityPtsNb(0),
	   lambda(0),
	   lambdaPtsNb(0),
     buffer_offset_index(0)
    {}

    //- copy ctor
    DataReturnValues( const DataReturnValues& src )
    { 
      *this = src;
    }

    //- operator=
    const DataReturnValues& operator=( const DataReturnValues& src )
    { 
      if (&src == this)
        return *this;
      
      intensity = src.intensity;
	    intensityPtsNb = src.intensityPtsNb;
	    lambda = src.lambda;
	    lambdaPtsNb = src.lambdaPtsNb;
      intensity_buf = src.intensity_buf;
      buffer_offset_index = src.buffer_offset_index;

      return *this;
    }

    //-intensity
    double * intensity;

    //- intensity pts nb
    int intensityPtsNb;

    //- lambda
    double * lambda;

    //- lambda pts nb
    int lambdaPtsNb;

    //- Data accumulation vector in continuous mode
    std::vector<DoubleBufPtr> intensity_buf;

    //- offset index corresponding to the number of erased buffer since entered the fifo mode
    std::size_t buffer_offset_index;

  } DataReturnValues;
  
	//- ctor
	SpectrometerTask (const Config& cfg);

	//- dtor
	virtual ~SpectrometerTask();

  //- starts task's periodic activity (a <tmo_ms> of 0 means asynchronous exec)
  void start_periodic_activity( size_t tmo_ms = 0 );

  //- stops task's periodic activity (a <tmo_ms> of 0 means asynchronous exec)
  void stop_periodic_activity( size_t tmo_ms = 0 );

  //- set the maximum buffer depth
  void set_max_buffer_depth( std::size_t depth );

  //- start the writing of the sequence
  void start( size_t tmo_ms = 0 );
    
  //- stop the writing of the sequence
  void stop();      
  //- snap 
  void snap( size_t tmo_ms = 0 );

	//- get the integration time
	long get_integration_time();

	//- get the averaging number
	unsigned short get_averaging_number();

	//- get the boxcar
	unsigned short get_boxcar();

	//- get the lamp enable
	bool get_lamp_enable();

	//- get the frames number
	unsigned short get_frames_number();

  //- set the integration time
  void set_integration_time( long iti_ms );

  //- set number of expected gates
  void set_nb_gates( std::size_t nb_gates );

	//- set re-acquisition delay (in ms)
	void set_acq_delay( std::size_t acq_delay );

	//- set the averaging number
	void set_averaging_number( unsigned short avg_nb );

	//- set the boxcar
	void set_boxcar( unsigned short box_nb );

	//- set the lamp enable
	void set_lamp_enable( bool lamp_bo );

	//- set the frames number
	void set_frames_number( unsigned short fr_nb );
    
  //- get the current data return values
  void get_data_return_values( DataReturnValues& dest );

  std::size_t get_acq_delay();
  std::size_t get_nb_gates();

  //- get the firmware version
  std::string get_firmware_version();

protected:
	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message( yat::Message& msg );

private:
  //- encapsulates the periodic activity
  void periodic_job_i();
    
  //- encapsulate the start function
    void start_i();
  
  //- encapsulate the stop function
    void stop_i();
  
  //- encapsulate the snap function
	void snap_i();

  // schedule a new acquisition when in "START" mode
  void schedule_next_acquisition();
  
  //- the task's configuration 
  Config m_cfg;
 
  //- the data return values
  DataReturnValues m_data;

  //- yat buffer for snap acquisition
  yat::Buffer<double*> m_buff_snap;

  //- snap boolean
  bool m_is_snap;

  //- acq boolean
  bool m_is_acq;

  //- frames number
  unsigned short m_frames_number;

  //- buffer depth
  std::size_t m_max_buffer_depth;

  //- number of expected gates in mode 2
  std::size_t m_nb_gates;

  //- Number of acquisition in START mode
  std::size_t m_acq_count;

  //- delay before starting new acquisition when START was invoked
  std::size_t m_acq_delay;

  //- Indicate the spectrometer is acquiring data (or waiting for next trigger)
  bool m_is_acquiring;

  yat::Time m_start_time;
  
  double m_period_sec;

#ifndef NO_DRIVER
  //-intensity java array
  DoubleArray m_intensity_array;

  //- lambda java array
  DoubleArray m_lambda_array;
#endif

#ifdef SIMULATION
  DoubleBuf m_intensity_array;
  DoubleBuf m_lambda_array;
  long           m_integration_time;
  unsigned short m_averaging_number;
#endif

  //- Mutex for data sharing
  yat::Mutex m_mtx;

};

} // namespace GroupManager_ns

#endif // _MY_DEVICE_TASK_H_
