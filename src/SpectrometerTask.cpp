//*******************************************************************************
//* Copyright (c) 2008-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************/
// ============================================================================
//
// = CONTEXT
//		SpectrometerTask
//
// = File
//		SpectrometerTask.h
//
// = AUTHOR
//    S.Gara
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <math.h>
#include <yat/threading/Mutex.h>
#include <yat/threading/Utilities.h>
#include <yat/time/Time.h>
#include "SpectrometerTask.h"

#ifdef SIMULATION
  #include <cstdlib>
  #include <ctime>
  #include <algorithm>
#endif

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
#define kSTART_PERIODIC_MSG	(yat::FIRST_USER_MSG + 1000)
#define kSTOP_PERIODIC_MSG	(yat::FIRST_USER_MSG + 1001)
#define kSTART_ACQ_MSG			(yat::FIRST_USER_MSG + 1002)
#define kSNAP_ACQ_MSG				(yat::FIRST_USER_MSG + 1003)
#define kDO_ACQ_MSG         (yat::FIRST_USER_MSG + 1004)
#define kSTOP_ACQ_MSG				(yat::FIRST_USER_MSG + 1005)

namespace Spectrometer_ns
{

// ----------------------------------------------------------------------------
// free function
// ----------------------------------------------------------------------------
void sleep_usec (unsigned long _usecs)
{
#define kNSECS_PER_SEC  1000000000.
#define kNSECS_PER_USEC 1000.

  unsigned long secs = 0;
  double nanosecs = kNSECS_PER_USEC * _usecs;

  while ( nanosecs >= kNSECS_PER_SEC )
  {
    secs += 1;
    nanosecs -= kNSECS_PER_SEC;
  }

  yat::ThreadingUtilities::sleep(secs, static_cast<unsigned long>(nanosecs));

#undef kNSECS_PER_MSEC
#undef kNSECS_PER_SEC
}

// ============================================================================
// SpectrometerTask::SpectrometerTask
// ============================================================================
SpectrometerTask::SpectrometerTask (const Config & cfg)
  : yat4tango::DeviceTask(cfg.host_device),
    m_cfg(cfg)
{
  //- configure optional msg handling
  enable_timeout_msg(false);
  enable_periodic_msg(false);
  set_periodic_msg_period(cfg.task_activity_period_ms);
  m_nb_gates = cfg.nb_gates;
  m_is_snap = false;
  m_is_acq = false;
  m_frames_number = 1;
  m_data.buffer_offset_index = 0;
  m_max_buffer_depth = 0;
  m_is_acquiring = false;
  m_acq_count = 0;
  m_acq_delay = 0;

#ifdef SIMULATION
  m_integration_time = 200000;
  m_averaging_number = 1;
  srand( static_cast <unsigned> (time(0)) );
#endif
}

// ============================================================================
// SpectrometerTask::~SpectrometerTask
// ============================================================================
SpectrometerTask::~SpectrometerTask()
{
  //- noop
}

// ============================================================================
// SpectrometerTask::process_message
// ============================================================================
void SpectrometerTask::process_message( yat::Message& msg )
{
  //- handle msg
  switch (msg.type())
  {
    //- THREAD_INIT ----------------------
    case yat::TASK_INIT:
      {
	      DEBUG_STREAM << "SpectrometerTask::handle_message::THREAD_INIT::thread is starting up" << std::endl;
      }
	    break;

	  //- THREAD_EXIT ----------------------
	  case yat::TASK_EXIT:
	    {
			  DEBUG_STREAM << "SpectrometerTask::handle_message::THREAD_EXIT::thread is quitting" << std::endl;
      }
		  break;

	  //- THREAD_PERIODIC ------------------
	  case yat::TASK_PERIODIC:
	    {
        DEBUG_STREAM << "SpectrometerTask::handle_message::THREAD_PERIODIC" << std::endl;
        //tâche périodique est réglée par kDO_ACQ_MSG
		    //periodic_job_i();
      }
	    break;

	  //- THREAD_TIMEOUT -------------------
	  case yat::TASK_TIMEOUT:
	    {
        //- not used here
      }
      break;

    //- kSTART_PERIODIC_MSG --------------
    case kSTART_PERIODIC_MSG:
      {
    	DEBUG_STREAM << "SpectrometerTask::handle_message::kSTART_PERIODIC_MSG" << std::endl;
      }
      break;

    //- kSTOP_PERIODIC_MSG ---------------
    case kSTOP_PERIODIC_MSG:
      {
    	DEBUG_STREAM << "SpectrometerTask::handle_message::kSTOP_PERIODIC_MSG" << std::endl;
      }
      break;

    //- kSTART_ACQ_MSG --------------
    case kSTART_ACQ_MSG:
      {
        DEBUG_STREAM << "SpectrometerTask::handle_message::kSTART_ACQ_MSG" << std::endl;
        start_i();
      }
      break;

    //- kSTOP_ACQ_MSG --------------
    case kSTOP_ACQ_MSG:
      {
    	  DEBUG_STREAM << "SpectrometerTask::handle_message::kSTOP_ACQ_MSG" << std::endl;
        stop_i();
      }
      break;

    //- kSNAP_ACQ_MSG --------------
    case kSNAP_ACQ_MSG:
      {
    	DEBUG_STREAM << "SpectrometerTask::handle_message::kSNAP_ACQ_MSG" << std::endl;
        snap_i();
      }
      break;

    //- kDO_ACQ_MSG --------------
    case kDO_ACQ_MSG:
      {
    	  DEBUG_STREAM << "SpectrometerTask::handle_message::kDO_ACQ_MSG" << std::endl;
		    periodic_job_i();
      }
      break;

    //- UNHANDLED MSG --------------------
		default:
		  DEBUG_STREAM << "SpectrometerTask::handle_message::unhandled msg type received" << std::endl;
			break;
  }
}

// ============================================================================
// SpectrometerTask::set_max_buffer_depth
// ============================================================================
void SpectrometerTask::set_max_buffer_depth( std::size_t depth )
{
  m_max_buffer_depth = depth;
}

// ============================================================================
// SpectrometerTask::start_periodic_activity
// ============================================================================
void SpectrometerTask::start_periodic_activity( size_t tmo_ms )
{
  try
  {
    //- synchronous approach: "post then wait for the message to be handled"
    if (tmo_ms)
      wait_msg_handled(kSTART_PERIODIC_MSG, tmo_ms);
    //- asynchronous approach: "post the data then return immediately"
    else
      post(kSTART_PERIODIC_MSG);
  }
  catch (const Tango::DevFailed&)
  {
    //- an exception could be thrown if the task msgQ is full (high water mark reached)
    //- in the synchronous case we could also caught an exception thrown by the code
    //- handling the message
    throw;
  }
}

// ============================================================================
// SpectrometerTask::stop_periodic_activity
// ============================================================================
void SpectrometerTask::stop_periodic_activity( size_t tmo_ms )
{
  try
  {
    //- synchronous approach: "post then wait for the message to be handled"
    if (tmo_ms)
      wait_msg_handled(kSTOP_PERIODIC_MSG, tmo_ms);
    //- asynchronous approach: "post the data then returm immediately"
    else
      post(kSTOP_PERIODIC_MSG);
  }
  catch (const Tango::DevFailed&)
  {
    //- an exception could be thrown if the task msgQ is full (high water mark reached)
    //- in the synchronous case we could also caught an exception thrown by the code
    //- handling the message
    throw;
  }
}

// ============================================================================
// SpectrometerTask::get_data_return_values
// ============================================================================
void SpectrometerTask::get_data_return_values( DataReturnValues& dest )
{
  yat::AutoMutex<> _lock(m_mtx);
  dest = m_data;
}

// ============================================================================
// SpectrometerTask::start
// ============================================================================
void SpectrometerTask::start( size_t tmo_ms )
{
  DEBUG_STREAM << "SpectrometerTask::start " << "tmo = " << tmo_ms << std::endl;
  try
  {
    //- synchronous approach: "post then wait for the message to be handled"
    if (tmo_ms)
      wait_msg_handled(kSTART_ACQ_MSG, tmo_ms);
    //- asynchronous approach: "post the data then return immediately"
    else
      post(kSTART_ACQ_MSG);
  }
  catch (const Tango::DevFailed&)
  {
    //- an exception could be thrown if the task msgQ is full (high water mark reached)
    //- in the synchronous case we could also caught an exception thrown by the code
    //- handling the message
    throw;
  }
}

// ============================================================================
// SpectrometerTask::snap
// ============================================================================
void SpectrometerTask::snap( size_t tmo_ms )
{
  try
  {
    m_cfg.host_device->set_state(Tango::RUNNING);

    //- synchronous approach: "post then wait for the message to be handled"
    if( tmo_ms )
      wait_msg_handled(kSNAP_ACQ_MSG, tmo_ms);
    //- asynchronous approach: "post the data then return immediately"
    else
      post(kSNAP_ACQ_MSG);
  }
  catch( const Tango::DevFailed& )
  {
    //- an exception could be thrown if the task msgQ is full (high water mark reached)
    //- in the synchronous case we could also caught an exception thrown by the code
    //- handling the message
    throw;
  }
}

// ============================================================================
// SpectrometerTask::periodic_job_i
// ============================================================================
#ifndef NO_DRIVER
void SpectrometerTask::periodic_job_i()
{
	int nb_spec = m_cfg.wrapper.highSpdAcq_GetNumberOfSpectraAcquired();
	int nb_points = 0;
	bool prob = false;
  bool l_saturated = false;

  //- check snap mode
	if (m_is_snap == true)
	{
    //- the number of frames is 1 in the spectrometer case, but the
    //- average mechanism is let, one never knows...
		//- check if all frames are acquired
		if (nb_spec == m_frames_number)
		{
			for (unsigned short idx = 0; idx <= m_frames_number-1; idx++)
			{
				//- check wether acquired spectrum is saturated
        l_saturated = m_cfg.wrapper.highSpdAcq_IsSaturated(idx);

        if ( (!l_saturated) ||                              // no saturation
             (l_saturated && !m_cfg.stop_acq_on_saturation)) // saturation but compute spectrum
				{
          //- get intensity
					m_intensity_array = m_cfg.wrapper.highSpdAcq_GetSpectrum(idx);
					m_buff_snap[idx] = m_intensity_array.getDoubleValues();
					nb_points = m_intensity_array.getLength();
				}
				else
				{
          prob = true;
          m_cfg.host_device->set_status("HARDWARE SATURATION - The acquired spectrum is saturated!");

          if (m_cfg.alarm_on_saturation)
          {
            m_cfg.host_device->set_state(Tango::ALARM);
          }
          else
          {
            m_cfg.host_device->set_state(Tango::FAULT);
          }

					THROW_DEVFAILED(
            _CPTC("HARDWARE SATURATION"),
						_CPTC("The acquired spectrum is saturated!"),
						_CPTC("SpectrometerTask::periodic_job_i "));
				}
      }

      if (prob == false)
      {
				//averaging
				for (unsigned short idx = 0; idx <= nb_points-1; idx++)
				{
					double val = 0.;

					for (unsigned short idy = 0; idy <= m_frames_number-1; idy++)
					{
						val += m_buff_snap[idy][idx];
					}
					m_buff_snap[0][idx] = val/double(m_frames_number);
				}

				//- get lambda
				m_lambda_array = m_cfg.wrapper.getWavelengths(m_cfg.spectrometer_index);

        { //- set data
          yat::AutoMutex<> _lock(m_mtx);
          m_data.intensity = m_buff_snap[0];
          m_data.intensityPtsNb = nb_points;
          m_data.lambda = m_lambda_array.getDoubleValues();
          m_data.lambdaPtsNb = m_lambda_array.getLength();
        }

        // set state & status
        if (l_saturated)
        {
          if (m_cfg.alarm_on_saturation)
          {
            m_cfg.host_device->set_state(Tango::ALARM);
            m_cfg.host_device->set_status("HARDWARE SATURATION - The acquired spectrum is saturated!");
          }
          else
          {
            m_cfg.host_device->set_state(Tango::STANDBY);
            m_cfg.host_device->set_status("TThe acquisition (saturated) is finished.");
          }
        }
        else
        {
          m_cfg.host_device->set_state(Tango::STANDBY);
          m_cfg.host_device->set_status("The acquisition is finished.");
        }
			}
		}
	}
	else // start mode
	{
		if (m_is_acq)
		{
			m_cfg.wrapper.highSpdAcq_AllocateBuffer(m_cfg.spectrometer_index, 1);


      if( !m_start_time.is_empty() )
      {
        yat::Time expected_next( m_start_time );
        expected_next.add_sec( m_acq_count * m_period_sec );
		int64 current_date = yat::CurrentTime().raw_value();
		if (expected_next.raw_value() > current_date)
		{
		  std::size_t sleep_time = expected_next.raw_value() - current_date;
          DEBUG_STREAM << "Sleep time (micro_secs): " << sleep_time << std::endl;
          sleep_usec( sleep_time );
		}
		else
		{
		  WARN_STREAM << "Device overruns: no waiting delay before next acquisition!" << std::endl;
		}
      }
      else
      {
        m_start_time.set_current();
      }

      ++m_acq_count;
      DEBUG_STREAM << "Acquisition time: " << yat::CurrentTime().to_inter() << std::endl;
      m_is_acquiring = true;
			m_cfg.wrapper.highSpdAcq_StartAcquisition(m_cfg.spectrometer_index);
      m_is_acquiring = false;

			nb_spec = m_cfg.wrapper.highSpdAcq_GetNumberOfSpectraAcquired();

      //- get data
			if (nb_spec == 1)
			{
        //- check wether acquired spectrum is saturated
        l_saturated = m_cfg.wrapper.highSpdAcq_IsSaturated(nb_spec-1);

        if ( (!l_saturated) ||                               // no saturation
             (l_saturated && !m_cfg.stop_acq_on_saturation)) // saturation but continue acquisition
				{
					m_intensity_array = m_cfg.wrapper.highSpdAcq_GetSpectrum(nb_spec-1);
					m_lambda_array = m_cfg.wrapper.getWavelengths(m_cfg.spectrometer_index);

          { //-  set data
            yat::AutoMutex<> _lock(m_mtx);
  					m_data.intensity = m_intensity_array.getDoubleValues();
  					m_data.intensityPtsNb = m_intensity_array.getLength();
  					m_data.lambda = m_lambda_array.getDoubleValues();
  					m_data.lambdaPtsNb = m_lambda_array.getLength();

            // put spectra in history buffer
            DoubleBufPtr buf_ptr = new DoubleBuf;
            buf_ptr->capacity(m_data.intensityPtsNb);
            buf_ptr->force_length(m_data.intensityPtsNb);
            *buf_ptr = m_data.intensity;
            m_data.intensity_buf.push_back(buf_ptr);

            if( m_max_buffer_depth > 0 && m_data.intensity_buf.size() > m_max_buffer_depth )
            {
              //- delete 1st element => fifo list
              m_data.intensity_buf.erase( m_data.intensity_buf.begin() );
              ++m_data.buffer_offset_index;
            }
          }

          // set state & status
          if (!l_saturated)
          {
            m_cfg.host_device->set_state(Tango::RUNNING);
            m_cfg.host_device->set_status("The acquisition is running...");
          }
          else
          {
            if (m_cfg.alarm_on_saturation)
            {
              m_cfg.host_device->set_state(Tango::ALARM);
              m_cfg.host_device->set_status("HARDWARE SATURATION - The acquired spectrum is saturated!");
            }
            else
            {
              m_cfg.host_device->set_state(Tango::RUNNING);
              m_cfg.host_device->set_status("The acquisition (saturated) is running...");
            }
          }

          schedule_next_acquisition();
				}
				else
				{
          // stop if saturated
          if (m_cfg.alarm_on_saturation)
          {
            m_cfg.host_device->set_state(Tango::ALARM);
          }
          else
          {
            m_cfg.host_device->set_state(Tango::FAULT);
          }

				  m_cfg.host_device->set_status("HARDWARE SATURATION - The acquired spectrum is saturated!");

				  THROW_DEVFAILED(
            _CPTC("HARDWARE SATURATION"),
					  _CPTC("The acquired spectrum is saturated!"),
					  _CPTC("SpectrometerTask::periodic_job_i "));
				}
			}
			else
			{
				m_cfg.host_device->set_state(Tango::FAULT);
				m_cfg.host_device->set_status("HARDWARE FAILURE - No spectra acquired!");

				THROW_DEVFAILED(
          _CPTC("HARDWARE FAILURE"),
					_CPTC("No spectra acquired!"),
					_CPTC("SpectrometerTask::periodic_job_i "));
			}
		}
		else
		{
			m_cfg.host_device->set_state(Tango::STANDBY);
			m_cfg.host_device->set_status("The acquisition is finished");
		}
	}
}
// ============================================================================
#elif defined(SIMULATION)
void SpectrometerTask::periodic_job_i()
{
  std::size_t nb_points = 1000;
  double max_rand = +10;
  double min_rand = -10;

  if( m_lambda_array.empty() )
  {
    m_lambda_array.capacity(nb_points);
    m_lambda_array.force_length(nb_points);

    // fill intensity array with random values
    for( std::size_t i = 0; i <nb_points; ++i )
      m_lambda_array[i] = 2 * i;
  }

  if( m_intensity_array.empty() )
  {
    m_intensity_array.capacity(nb_points);
    m_intensity_array.force_length(nb_points);
  }

  // fill intensity array with random values
  for( std::size_t i = 0; i <nb_points; ++i )
  {
    if( 0 == i )
      m_intensity_array[0] = 50.0 + min_rand + static_cast<double>(std::rand()) / (static_cast<double>(RAND_MAX/(max_rand - min_rand)));
    else do
    {
      m_intensity_array[i] = m_intensity_array[i-1] + min_rand + static_cast<double>(std::rand()) / (static_cast<double>(RAND_MAX/(max_rand - min_rand)));
    }
    while( m_intensity_array[i] < 0 );
  }

  //- check snap mode
  if (m_is_snap == true)
  {
    { //- set data
      yat::AutoMutex<> _lock(m_mtx);
      m_data.intensity = m_intensity_array.base();
      m_data.intensityPtsNb = nb_points;
      m_data.lambda = m_lambda_array.base();
      m_data.lambdaPtsNb = nb_points;
    }

    m_cfg.host_device->set_state(Tango::STANDBY);
    m_cfg.host_device->set_status("The acquisition is finished");
  }
  else // start mode
  {
    if (m_is_acq)
    {
      // Integration time simulated through a time delay
      yat::ThreadingUtilities::sleep(0, get_integration_time() * 1000000);

      DoubleBufPtr buf_ptr = new DoubleBuf;
      buf_ptr->capacity(nb_points);
      *buf_ptr = m_intensity_array.base();
      ++m_acq_count;

      //-  set data
      yat::AutoMutex<> _lock(m_mtx);
      m_data.intensity = m_intensity_array.base();
      m_data.intensityPtsNb = nb_points;
      m_data.lambda = m_lambda_array.base();
      m_data.lambdaPtsNb = nb_points;
      m_data.intensity_buf.push_back(buf_ptr);

      if( m_max_buffer_depth > 0 && m_data.intensity_buf.size() > m_max_buffer_depth )
      {
        //- delete 1st element => fifo list
        m_data.intensity_buf.erase( m_data.intensity_buf.begin() );
        ++m_data.buffer_offset_index;
      }

      schedule_next_acquisition();
    }
    else
    {
      m_cfg.host_device->set_state(Tango::STANDBY);
      m_cfg.host_device->set_status("The acquisition is finished");
    }
  }
}
#endif

// ============================================================================
// SpectrometerTask::schedule_next_acquisition
// ============================================================================
void SpectrometerTask::schedule_next_acquisition()
{
  if( 0 == m_nb_gates || (m_nb_gates > 0 && m_acq_count < m_nb_gates) )
  {
    post(kDO_ACQ_MSG);
  }
  else
  {
    m_cfg.host_device->set_status("The spectrometer is ready");
    m_cfg.host_device->set_state(Tango::STANDBY);
  }
}

// ============================================================================
// SpectrometerTask::start_i
// ============================================================================
void SpectrometerTask::start_i()
{
	DEBUG_STREAM << "SpectrometerTask::start_i" << std::endl;
  m_cfg.host_device->set_state(Tango::RUNNING);
  m_cfg.host_device->set_status("The acquisition is running...");
  m_is_snap = false;
  m_is_acq = true;
  m_data.intensity_buf.clear();
  m_data.buffer_offset_index = 0;
  m_acq_count = 0;
  m_period_sec = double( get_integration_time() * get_averaging_number() + m_acq_delay ) / 1000.0;

  m_start_time.set_empty();
  DEBUG_STREAM << "period (sec) = " << m_period_sec << std::endl;

  post(kDO_ACQ_MSG);
}

// ============================================================================
// SpectrometerTask::stop
// ============================================================================
void SpectrometerTask::stop()
{
  wait_msg_handled(kSTOP_ACQ_MSG);
}

// ============================================================================
// SpectrometerTask::stop_i
// ============================================================================
void SpectrometerTask::stop_i()
{
	DEBUG_STREAM << "SpectrometerTask::stop" << std::endl;
	m_is_acq = false;
}

// ============================================================================
// SpectrometerTask::snap_i
// ============================================================================
void SpectrometerTask::snap_i()
{
	DEBUG_STREAM << "SpectrometerTask::snap_i" << std::endl;

  //- snap
	m_cfg.host_device->set_state(Tango::RUNNING);
	m_cfg.host_device->set_status("The device is snapping...");
	m_is_snap = true;

  //- buffer preparation
#ifndef NO_DRIVER
	m_buff_snap.capacity(m_frames_number);
	m_buff_snap.force_length(m_frames_number);
	m_cfg.wrapper.highSpdAcq_AllocateBuffer(m_cfg.spectrometer_index, m_frames_number);
	m_cfg.wrapper.highSpdAcq_StartAcquisition(m_cfg.spectrometer_index);
	int m_nb_spec = m_cfg.wrapper.highSpdAcq_GetNumberOfSpectraAcquired();
	if (m_nb_spec == 0)
	{
		m_cfg.host_device->set_state(Tango::FAULT);
		m_cfg.host_device->set_status("HARDWARE FAILURE - The spectrum is not present");
		THROW_DEVFAILED(_CPTC("HARDWARE FAILURE"),
			_CPTC("The spectrum is not present"),
			_CPTC("SpectrometerTask:: "));
	}
#endif
	post(kDO_ACQ_MSG);
}
// ============================================================================
// SpectrometerTask::get_integration_time
// ============================================================================
long SpectrometerTask::get_integration_time()
{
  //- get the integration time
#ifndef NO_DRIVER
	long lTime = m_cfg.wrapper.getIntegrationTime(m_cfg.spectrometer_index);
#elif defined (SIMULATION)
  long lTime = m_integration_time;
#endif

	return (lTime / 1000);
}

// ============================================================================
// SpectrometerTask::get_averaging_number
// ============================================================================
unsigned short SpectrometerTask::get_averaging_number()
{
  //- get the averaging number
#ifndef NO_DRIVER
	unsigned short lAvgNb = m_cfg.wrapper.getScansToAverage(m_cfg.spectrometer_index);
#elif defined (SIMULATION)
  unsigned short lAvgNb = m_averaging_number;
#endif

	return lAvgNb;
}


// ============================================================================
// SpectrometerTask::get_frames_number
// ============================================================================
unsigned short SpectrometerTask::get_frames_number()
{
	return m_frames_number;
}

// ============================================================================
// SpectrometerTask::get_boxcar
// ============================================================================
unsigned short SpectrometerTask::get_boxcar()
{
  //- get the boxcar
#ifndef NO_DRIVER
	unsigned short lBox = m_cfg.wrapper.getBoxcarWidth(m_cfg.spectrometer_index);
#elif defined (SIMULATION)
  unsigned short lBox = 1;
#endif

	return lBox;
}

// ============================================================================
// SpectrometerTask::get_lamp_enable
// ============================================================================
bool SpectrometerTask::get_lamp_enable()
{
  //- get the lamp enable
#ifndef NO_DRIVER
	bool is_enabled = m_cfg.wrapper.getStrobeEnable(m_cfg.spectrometer_index);
#elif defined (SIMULATION)
  bool is_enabled = false;
#endif

	return is_enabled;
}
// ============================================================================
// SpectrometerTask::set_integration_time
// ============================================================================
void SpectrometerTask::set_integration_time( long p_time )
{
#ifndef NO_DRIVER
  //- set the integration time (in microsec)
	m_cfg.wrapper.setIntegrationTime(m_cfg.spectrometer_index, 1000 * p_time);
#elif defined (SIMULATION)
  m_integration_time = p_time * 1000;
#endif
}

// ============================================================================
// SpectrometerTask::set_averaging_number
// ============================================================================
void SpectrometerTask::set_averaging_number( unsigned short p_val )
{
#ifndef NO_DRIVER
  //- set the averaging number
  m_cfg.wrapper.setScansToAverage(m_cfg.spectrometer_index, p_val);
#elif defined (SIMULATION)
  m_averaging_number = p_val;
#endif
}

// ============================================================================
// SpectrometerTask::set_acq_delay
// ============================================================================
void SpectrometerTask::set_acq_delay( std::size_t acq_delay )
{
  m_acq_delay = acq_delay;
}

// ============================================================================
// SpectrometerTask::set_nb_gates
// ============================================================================
void SpectrometerTask::set_nb_gates( std::size_t nbGates )
{
  m_nb_gates = nbGates;
}

// ============================================================================
// SpectrometerTask::get_nb_gates
// ============================================================================
std::size_t SpectrometerTask::get_nb_gates()
{
  return m_nb_gates;
}

// ============================================================================
// SpectrometerTask::get_acq_delay
// ============================================================================
std::size_t SpectrometerTask::get_acq_delay()
{
  return m_acq_delay;
}

// ============================================================================
// SpectrometerTask::set_frames_number
// ============================================================================
void SpectrometerTask::set_frames_number( unsigned short p_val )
{
	if (p_val == 0)
	{
		THROW_DEVFAILED(
      _CPTC("DATA_OUT_OF_RANGE"),
		  _CPTC("Frame number out of range"),
		  _CPTC("Set_frames_number"));
	}

	//- set the frames number
  m_frames_number = p_val;
}

// ============================================================================
// SpectrometerTask::set_boxcar
// ============================================================================
void SpectrometerTask::set_boxcar( unsigned short p_val )
{
#ifndef NO_DRIVER
  //- set the boxcar
	m_cfg.wrapper.setBoxcarWidth(m_cfg.spectrometer_index, p_val);
#endif
}

// ============================================================================
// SpectrometerTask::set_lamp_enable
// ============================================================================
void SpectrometerTask::set_lamp_enable( bool p_val )
{
#ifndef NO_DRIVER
  //- set the lamp enable
	m_cfg.wrapper.setStrobeEnable(m_cfg.spectrometer_index, p_val);
#endif
}

// ============================================================================
// SpectrometerTask::get_firmware_version
// ============================================================================
std::string SpectrometerTask::get_firmware_version()
{
#ifndef NO_DRIVER
	//- get the firmware version
	JString mFirm;
  std::string vers;

	mFirm = m_cfg.wrapper.getFirmwareVersion(m_cfg.spectrometer_index);
	vers.assign(mFirm.getASCII());
  return (vers);
#elif defined(SIMULATION)
  return "Simulation";
#endif
}

} // namespace

